import React, { Component } from "react";
import {Button, Card, Form, ListGroup} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Result from "./Result";
import '../index.css';
export default class calulator extends Component{
    constructor(props){
        super(props);
        this.state ={
            numbers: []
              };
        this.eventSunmit= this.eventSunmit.bind(this);
       
    }
    eventSunmit(event){
        event.preventDefault();
        var num1 = Number(this.number1.value);
        var num2 = Number(this.number2.value);
        var option = this.option.value;
        var result;

        switch(option){
            case "+":
                result = num1 + num2;
            break;
            case "-":
                result = num1 - num2;
                break;
            case "*":
                result = num1 * num2;
                break;
            case "/":
                result = num1 / num2;
                break;
            case "%":
                result = num1 % num2;
                break;
            default:
                alert("please choose  operator");
                break;
        }
        this.setState({
            numbers : this.state.numbers.concat(result),

        });

    }
    render(){
        var item = this.state.numbers.map((element, index)=>(
            <Result output={element} key={index}/>
        )
        );
        return(
            <div>
                <form onSubmit={this.eventSunmit}>
                    <div className="container App">
                            <div className="col=md-4">
                                <Card> 
                                 <Card.Body>
                                        <Form.Group>
                                            <Form.Control
                                            type="number"
                                            ref={(num1) => (this.number1 = num1)}
                                            name ="number1"
                                            />
                                        </Form.Group>

                                        <Form.Group>
                                            <Form.Control
                                            type="number"
                                            ref={(num2) => (this.number2 = num2)}
                                            name ="number2"
                                            />
                                        </Form.Group>
                                    <Form.Group controlId="">
                            <Form.Control as ="select" custom ref={(choose)=>(this.option=choose)} name="option">
                            <select>
                                <option value="+">+ Plus</option>
                                <option value="-">- Sub</option>
                                <option value="*">*  Mul</option>
                                <option value="%">% Module</option>
                                <option value="/">/ Div</option>
                            </select> 
                            </Form.Control>
                            </Form.Group>
                            <Button variant="primary" type="submit">Calulate
                            </Button>
                                    </Card.Body>

                                </Card>
                            </div>
                            <div className="col=md-12">
                                <h2>Result History</h2>
                                <br/>
                                <ListGroup>{item}</ListGroup>
                            </div>
                        
                    </div>
                </form>
            </div>
        )
    
    }
  
 
}